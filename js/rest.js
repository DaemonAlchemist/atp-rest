/**
 * Created by Andy on 8/25/2016.
 */

class Rest {
    constructor() {
        this.restBase = "";
        this.loginToken = localStorage.getItem('atp.rest.loginToken') || '';
        if(this.loginToken) {
            console.log("Login token: " + this.loginToken);
        }
    }

    connect(url) {
        this.restBase = url;
    }

    setLoginToken(token) {
        this.loginToken = token;
        localStorage.setItem('atp.rest.loginToken', token);
    }

    getLoginToken() {
        return this.loginToken;
    }

    isLoggedIn() {
        return this.loginToken.length > 0;
    }

    call(url, options) {
        var $this = this;
        return new Promise((resolve, reject) => {
            options = $.extend({}, {
                url: $this.restBase + url,
                method: 'get',
                dataType: 'json',
                processData: false,
                contentType: 'application/json',
                cache: false,
                data: {},
                headers: {
                    loginToken: $this.loginToken
                },
                success: (data) => {
                    if(typeof data.messages != "undefined" && data.messages.length > 0) {
                        $(document).trigger('atp.messages.new', [data.messages]);
                    }
                    console.log("REST results: " + JSON.stringify(data));
                    resolve(data.results);
                },
                error: (jqXHR, textStatus, errorThrown) => {
                    let data = $.parseJSON(jqXHR.responseText);
                    if(typeof data.messages != "undefined" && data.messages.length > 0) {
                        $(document).trigger('atp.messages.new', [data.messages]);
                    }
                    console.log("REST error: " + JSON.stringify(data));
                    reject(jqXHR, textStatus, errorThrown);
                }
            }, options);
            if(options.method == 'get') {
                options.url += "?" + $.param(options.data);
                delete options.data;
            } else {
                options.data = JSON.stringify(options.data);
            }
            $.ajax(options);
        });
    }

    get(url, options) {return this.call(url, $.extend({}, options, {method: 'get'}));}
    post(url, options) {return this.call(url, $.extend({}, options, {method: 'post'}));}
    put(url, options) {return this.call(url, $.extend({}, options, {method: 'put'}));}
    delete(url, options) {return this.call(url, $.extend({}, options, {method: 'delete'}));}
    patch(url, options) {return this.call(url, $.extend({}, options, {method: 'patch'}));}
}

var rest = new Rest();

$.fn.restForm = function(options) {
    let $this = $(this);

    return new Promise((resolve, reject) => {
        $this.find('button[type=submit]').click(function() {
            let form = $this.closest("form");
            let data = form.serializeJSON();
            let method = form.attr('method');
            console.log("Submitting rest form with: " + JSON.stringify(data));
            rest.call(form.data('url'), $.extend({}, options, {data, method}))
                .then(resolve)
                .catch(reject);
            return false;
        });
    });
};

export default rest;